__author__ = 'Prpht'

import properties
import random
from SimPy import SimulationStep
import time

class Cell:
    def __init__(self, hex, id, ecosystem, sim):
        self.hex = hex
        self.id = id
        self.ecosystem = ecosystem
        self.organism = None
        self.alive = False
        self.vicinity = None
        self.vicinity_others = None
        self.lock = SimulationStep.Resource(sim=sim)
        self.to_specialize = None

    def despecialize(self):
        self.hex.set_marker_colour(None)
        self.to_specialize = random.randint(5,10) #TODO do properties

    def specialize(self, chances):
        self.to_specialize = None
        if random.random() < chances[self.vicinity]:
            self.hex.set_marker_colour(properties.LIGHT_BROWN)
        else:
            self.hex.set_marker_colour(properties.BRIGHT_GREEN)

    def born(self, colour):
        self.alive = True
        self.update_vicinity()
        for cell in self.ecosystem.vicinity(self):
            cell.update_vicinity()
        self.despecialize()
        self.hex.set_colour(colour)

    def die(self):
        self.alive = False
        for cell in self.ecosystem.vicinity(self):
            cell.update_vicinity()
        self.hex.set_colour(properties.BRIGHT_RED)

    def update_vicinity(self):
        self.vicinity_others = self.vicinity_status(False)
        self.vicinity = self.vicinity_status(True)

    def vicinity_status(self, consider_others):
        if not self.alive: pass
        if consider_others:
            free_cells = [cell for cell in self.ecosystem.vicinity(self) if not cell.alive or cell.organism is not self.organism]
        else:
            free_cells = [cell for cell in self.ecosystem.vicinity(self) if not cell.alive]
        free_no = len(free_cells)
        if free_no == 6:
            return properties.CellVicinity.NO
        elif free_no == 5:
            return properties.CellVicinity.SINGLE
        elif free_no == 1:
            return properties.CellVicinity.FIVE
        elif free_no == 0:
            return properties.CellVicinity.FULL
        else:
            vicinity_sum = lambda x : sum([len([c for c in x if c.in_vicinity(fc)]) for fc in x])
            average_id = lambda x: (sum([a for a, _ in [c.id for c in x]])/len(x), sum([b for _, b in [c.id for c in x]])/len(x))
            v_sum = vicinity_sum(free_cells)
            if free_no == 4:
                if v_sum == 6:
                    return properties.CellVicinity.PAIR_CLOSE
                elif v_sum == 4:
                    av_id = average_id(free_cells)
                    if av_id == self.id:
                        return properties.CellVicinity.PAIR_LINE
                    else:
                        return properties.CellVicinity.PAIR_GAP
            elif free_no == 3:
                if v_sum == 4:
                    return properties.CellVicinity.TRI_CLOSE
                elif v_sum == 2:
                    return properties.CellVicinity.TRI_MIXED
                elif v_sum == 0:
                    return properties.CellVicinity.TRI_SYMMETRY
            elif free_no == 2:
                if v_sum == 2:
                    return properties.CellVicinity.FOUR_CLOSE
                elif v_sum == 0:
                    av_id = average_id(free_cells)
                    if av_id == self.id:
                        return properties.CellVicinity.FOUR_LINE
                    else:
                        return properties.CellVicinity.FOUR_GAP

    def in_vicinity(self, cell):
        return cell in self.ecosystem.vicinity(self)

class Organism(SimulationStep.Process):
    start_green = 111
    id = 1

    def __init__(self, ecosystem, sim):
        self.ecosystem = ecosystem
        self.colour = (20, Organism.start_green, 20)
        Organism.start_green = (Organism.start_green + 44) % 150
        self.id = Organism.id
        Organism.id += 1
        self.cells = list()
        if self.id % 2 is 0:
            self.grow_chance = properties.BLOB[0] #bloby
            self.cuticula_chance = properties.BLOB[1]
        else:
            self.grow_chance = properties.TREE[0] #nitkowce
            self.cuticula_chance = properties.TREE[1]
        SimulationStep.Process.__init__(self, sim=sim)

    def fitness(self):
        pass

    def live(self, lifetime):
        print("Organism was born at %s!" % (self.sim.now()))
        cell = random.choice(list(self.ecosystem.cells.values()))
        yield SimulationStep.request, self, cell.lock
        self.add_cell(cell)
        # yield SimulationRT.hold, self, lifetime
        while True:
            yield SimulationStep.hold, self, 0.05
            self._sleep()
            for cell in [cell for cell in self.cells if cell.to_specialize is not None]:
                cell.to_specialize -= 1
                if cell.to_specialize is 0:
                    cell.specialize(self.cuticula_chance)
            cell = self.grow()
            try:
                yield (SimulationStep.request, self, cell.lock), (SimulationStep.hold, self, 0.05)
                if self.acquired(cell.lock):
                    self.add_cell(cell)
                else:
                    self._sleep()
            except AttributeError:
                pass
        self.remove_cells()
        print("Organism died at %s!" % (self.sim.now()))

    def _sleep(self):
        time.sleep(abs(random.gauss(properties.TIME_MU, properties.TIME_SIGMA)))

    def grow(self):
        chances = [int(self.grow_chance[cell.vicinity_others] * (cell.light/properties.LIGHT_MAX) + cell.light - properties.LIGHT_MIN) for cell in self.cells]
        for i in range(len(chances)):
            if self.cells[i].vicinity_others is properties.CellVicinity.FULL: chances[i] = 0
        chances_sum = sum(chances)
        if chances_sum is 0:
            chances = [int(self.grow_chance[cell.vicinity_others] * (cell.light/properties.LIGHT_MAX) + cell.light - properties.LIGHT_MIN + 1) for cell in self.cells]
            for i in range(len(chances)):
                if self.cells[i].vicinity_others is properties.CellVicinity.FULL: chances[i] = 0
            chances_sum = sum(chances)
        if chances_sum is 0:
            return None
        chosen = random.randint(1, chances_sum)
        acc = 0
        i = -1
        while chosen > acc:
            acc += chances[i+1]
            i += 1
        try:
            to_grow = random.choice([cell for cell in self.ecosystem.vicinity(self.cells[i]) if not cell.alive])
        except IndexError:
            return None
        self.cells[i].despecialize()
        return to_grow

    def add_cell(self, cell):
        self.cells.append(cell)
        cell.organism = self.id
        cell.born(self.colour)

    def remove_cells(self):
        for cell in self.cells:
            cell.die()
            yield SimulationStep.release, self, cell.lock
        self.cells = list()