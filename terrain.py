__author__ = 'Prpht'

import itertools
import math
import properties
import pygame
from pygame import draw
from pygame import mouse

class Hex:
    def __init__(self, surface, x, y, r, colour=properties.TEAL, border_colour=None, marker_colour=None):
        self.surface = surface
        self.x = x
        self.y = y
        self.r = r
        self.points = self._points(x, y, self.r)
        self.marker_points = self._points(x, y, self.r/4)
        self.border_points = self._points(x, y, self.r*0.80)
        self.colour = colour
        self.border_colour = border_colour
        self.marker_colour = marker_colour
        self.id = None

    def draw(self):
        draw.polygon(self.surface, self.colour, self.points)
        if self.border_colour is not None:
            draw.polygon(self.surface, self.border_colour, self.border_points, 2)
        if self.marker_colour is not None:
            draw.polygon(self.surface, self.marker_colour, self.marker_points)


    def _points(self, x, y, r):
        return [(x, y - r),
                (x + r*math.sqrt(3)/2, y - r/2),
                (x + r*math.sqrt(3)/2, y + r/2),
                (x, y + r),
                (x - r*math.sqrt(3)/2, y + r/2),
                (x - r*math.sqrt(3)/2, y - r/2)]

    def set_colour(self, colour):
        self.colour = colour
        self.draw()

    def set_border_colour(self, border_colour):
        self.border_colour = border_colour
        self.draw()

    def set_marker_colour(self, marker_colour):
        self.marker_colour = marker_colour
        self.draw()

    def hexes_in_radius(self, radius):
        hexes = []
        x,y = self.id
        for i in range(radius):
            hexes.append((x-radius+i, y+i)) # top left to top right
            hexes.append((x+i, y+radius)) # top right to right
            hexes.append((x+radius, y+radius-i)) # right to bottom right
            hexes.append((x+radius-i, y-i)) # bottom right to bottom left
            hexes.append((x-i, y-radius)) # bottom left to left
            hexes.append((x-radius, y-radius+i)) # left to top left
        return hexes

class Terrain:
    def __init__(self, surface, edge_size):
        self.hexes = dict()
        max_index = 2*edge_size-1
        hex_width = (properties.WIDTH-properties.HEX_MARGIN)/max_index
        hex_radius = hex_width/math.sqrt(3)
        self.x_dist = hex_width / 2
        self.y_dist = 1.5 * hex_radius
        new_hex = Hex(surface, properties.WIDTH/2, properties.HEIGHT/2, hex_radius)
        self.hexes[(0,0)] = new_hex
        new_hex.id = (0,0)
        created_hexes = set(self.hexes.keys())
        for _ in range(edge_size - 1):
            fresh_hexes = set()
            for hex, (old_up, old_down) in [(self.hexes[id], id) for id in created_hexes]:
                for up, down in [(up, down) for (up, down) in itertools.product([-1,0,1], [-1,0,1]) if up+down != 0]:
                    new_id = (old_up+up, old_down+down)
                    if new_id not in self.hexes:
                        new_x = hex.x + (up + down) * self.x_dist
                        new_y = hex.y + (up - down) * self.y_dist
                        self.hexes[new_id] = Hex(surface, new_x, new_y, hex_radius)
                        self.hexes[new_id].id = new_id
                        fresh_hexes.add(new_id)
            created_hexes = fresh_hexes

    def draw(self):
        for hex in self.hexes.values():
            hex.draw()


    def hex_under_mouse(self):
        pygame.event.wait().type == pygame.MOUSEMOTION
        (mouse_x, mouse_y) = mouse.get_pos()
        mouse_x -= properties.WIDTH/2
        mouse_y -= properties.HEIGHT/2
        x_mov = round((self.y_dist*mouse_x + self.x_dist*mouse_y)/(2*self.x_dist*self.y_dist))
        y_mov = round((self.y_dist*mouse_x - self.x_dist*mouse_y)/(2*self.x_dist*self.y_dist))
        id = (x_mov, y_mov)
        return self.hexes[id] if id in self.hexes else None