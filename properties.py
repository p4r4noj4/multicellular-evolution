__author__ = 'p4r4noj4'

#colours
TEAL = (0,128,128)
OLIVE = (100, 100, 0)
BRIGHT_GREEN = (0, 255, 0)
BRIGHT_BLUE = (0, 0, 255)
BRIGHT_RED = (255, 0, 0)
BRIGHT_YELLOW = (255, 255, 102)
YELLOW = (255, 255, 0)
BLACK = (0, 0, 0)
LIGHT_BROWN = (110, 64, 32)

#window constants
HEXES = 25
WIDTH = 900
HEIGHT = 800
HEX_MARGIN = 20
TIME_MU = 0.02
TIME_SIGMA = 0.01


#simulation constants
ORGANISMS = 3
AVERAGE_LIFETIME = 10.0
LIFETIME_DEVIATION = 2.0
SIMULATION_TIME = 20.0

# environment constants
LIGHT_SOURCES = 3
LIGHT_MAX = 1000
LIGHT_MIN = 10
LIGHT_DEGRADATION = 50
LIGHT_MAX_SIZE = 5
LIGHT_MIN_SIZE = 2
LIGHT_RINGS = 3

BLOB = [[1, 1, 1, 24, 672, 1, 24, 672, 1, 24, 62, 1, 0],
        [0.0, 0.5, 0.9, 0.7, 0.5, 0.9, 0.1, 0.2, 0.9, 0.1, 0.1, 0.05, 0.0]]
TREE = [[1, 1000, 500, 1, 1, 100, 20, 1, 20, 1, 1, 1, 0],
        [0.0, 0.5, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.2, 0.2, 0.05, 0.0]]

#enum type definition
#>>> Numbers = enum('ZERO', 'ONE', 'TWO')
#>>> Numbers.ZERO
#0
def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type("Enum", (), enums)

CellVicinity = enum("NO", "SINGLE", "PAIR_CLOSE", "PAIR_GAP", "PAIR_LINE", "TRI_CLOSE", "TRI_MIXED", "TRI_SYMMETRY", "FOUR_CLOSE", "FOUR_GAP", "FOUR_LINE", "FIVE", "FULL")

CellType = enum("CUTICULA", "CHLOROPLAST")

EnvType = enum("SUN", "WATER", "NONE")