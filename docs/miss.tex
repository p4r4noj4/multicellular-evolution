\documentclass[a4paper]{article}

\usepackage{amsmath,amssymb}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[MeX]{polski}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{latexsym}
\usepackage{seqsplit}
\usepackage{indentfirst}
\usepackage{microtype}
\usepackage[section]{placeins}
\usepackage[lined,linesnumbered,boxed,commentsnumbered,ruled]{algorithm2e}
\usepackage[colorlinks=true, pagebackref=true]{hyperref}
\usepackage{mathtools}

\begin{document}

\title{Modelowanie i Symulacja Systemów}
\author{Igor~Jurkowski \and Radosław~Łazarz}

\date{\today}
\maketitle

\section{Wstęp}
Celem stworzonej aplikacji było umożliwienie symulacji wzrostu prostych organizmów wielokomórkowych. Problemem, na którym się skupiono, była próba weryfikacji, czy organizm którego genom jest homogeniczny (brak różnic w informacji posiadanej przez poszczególne komórki) i który nie posiada cetralnego mechanizmu sterującego jego wzrostem będzie w stanie wytworzyć złożone ciało o nietrywialnym kształcie.

Zdecydowano się na badanie organizmów o cechach roślinnych, ze względu na możliwość pominięcia kwestii związanych z poruszaniem się (trudnych do zamodelowania, nie mających jednakże związku z planowanych eksperymentem). Założono, iż genotyp opisuje strategie rozrostu i specjacji komórki zależną od jej najbliższego otoczenia, jak również czynników środowiskowych (takich jak nasłonecznienie).

\section{Wizualizacja}

W celu uproszczenia modelu, na starcie prac założono, iż symulacja będzie miała charakter dyskretny i będzie odbywać się na dwuwymiarowej płaszczyźnie podzielonej równomiernie na sześciokąty foremne (tzw. ``hexy''). W obrębie jednego z nich warunki zewnętrzne są stałe. Pojedyńcza komórka zajmuje cały hex, może sąsiadować z maksymalnie sześcioma innymi komórkami.

\begin{figure}[h!]
  \centering
    \includegraphics[width=0.5\textwidth]{2.png}
   \caption{Próbka możliwości systemu wizualizującego środowisko i organizmy.}
\end{figure}

By móc obserwować przebieg symulacji przygotowano proste środowisko wizualizacyjne. Zostało ono stworzone od podstaw z wykorzystaniem biblioteki PyGame (użytej do obsługi renderowania grafiki 2D). Pozwala ono na generowanie planszy o zadanym rozmiarze, bezpośrednie odwoływanie się do każdego z heksów, nadawanie im kolorów (by oznaczyć do którego organizmu należy dana komórka), obramowań (by przedstawić informację o specjalnych cechach środowiska w danym miejscu) oraz znaczników (by wyróżnić komórki o różnych specjalizacjach). Umożliwia również szybkie odnajdywanie sąsiednich heksów lub wyznaczanie ich zbiorów w kształcie pierścienia lub okręgu o zadanym środku i średnicy.

Implementację ułatwiło przyjęte adresowanie. Ustalono, iż centralny heks ma współrzędne $(0,0)$. Wektor $[1,0]$ odpowiada przesunięciu się o jeden heks w kierunku w-prawo-i-do-góry. Wektor $[0,1]$ odpowiada natomiast za kierunek w-prawo-i-w-dół. Składając je uzyskujemy wektor $[1,1]$ - ruch w prawo. Wektory przeciwne do nich opisują pozostałe możliwe ruchy (w lewo). Adresowanie takie jest jednoznaczne i znacząco ułatwia wyznaczanie obszarów sąsiednich, jak również opis dowolnych ruchów na planszy. Jest to znaczące usprawnienie w porównaniu z najbardziej klasycznym sposobem opisu planszy składającej się z heksów (zakładającym jej podział na wiersze i lekko skośne kolumny oraz przyporządkowanie im odpowiedniej współrzędnej).

\section{Symulacja}

Decyzje podejmowane podczas projektowania symulacji miały na celu odnalezienie kompromisu między chęcią jak najlepszego odwzorowania modelowanej sytuacji a potrzebą możliwie skutecznej redukcji ilości stopni swobody. Postanowiono, iż równoczesnej symulacji podlegać będzie od jednego do kilku organizmów, rozpoczynających jako pojedyńcze komórki, następnie rozrastających się, zachowując jednakże spójną i ciągłą strukturę. Każda symulowana komórka rozpoczyna życia jako komórka macierzysta. Po upłynięciu losowej ilości czasu ulega ona specjacji, której kierunek (szansa \% na dany wariant) jest określony w genomie jako funkcja warunków środowiskowych i otoczenia. Zdecydowano o uwzględnieniu dwóch typów komórek - fotosyntezujących i wzmacniających. Każda istniejąca komórka, która posiada w otoczeniu wolny heks może ulec podziałowi, tracąc swoją specjację i zyskując jednego sąsiada. Szansa na to, iż dana komórka ulegnie podziałowi jest również opisana w genomie i uzależniona od tych samych czynników, co specjacja. Pominięto aspekt śmiertelności komórek. W przypadku konfliktu między organizmami obowiązuje zasada pierwszeństwa - heks zajęty przed daną roślinę pozostaje jej częścią do końca symulacji.

\begin{figure}[h!]
  \centering
    \includegraphics[width=0.5\textwidth]{1.png}
   \caption{Rozróżniane rodzaje otoczenia pojedyńczej komórki.}
   \label{fig:CellKinds}
\end{figure}

W celu uproszczenia symulacji czynniki środowiskowe zostały ograniczone jedynie do stopnia nasłonecznienia, będącego cechą każdego z heksów. W czasie eksperymentów z modelem było ono modelowane jako spójne koncentryczne strefy, z najbardziej nasłonecznionymi obszarami położonymi w ich centrum. Dodatkowym czynnikiem wpływającym na zachowanie komórek było ich otoczenie. Zidentyfikowano 13 możliwych rodzajów sąsiedztwa (zaprezentowano je na Rysunku~\ref{fig:CellKinds}). Sąsiedztwa różnią się ze względu na ilość i rozmieszczenie sąsiadów (równomierne lub skupione po jednej stronie komórki). Są natomiast nierozróżnialne ze względu na obroty oraz symetrię osiową (tzw. ``odbicie lustrzane'').

Przygotowano dwa rodzaje genomów. Jeden opisywał organizm o charakterze ``bulwiastym'', mającym tendencję do równomiernego wzrostu we wszystkich kierunkach, oraz wytworzeniu ``otoczki'' z komórek wzmacniających i wielu komórek przeprowadzających fotosyntezę wewnątrz. Drugim był organizm o charakterze ``drzewiastym'', mający tendencję do wzrostu w postaci długich, cienkich ``wypustek'' o wysokiej zawartości komórek wzmacniających i gwałtownego przyśpieszania podziałów w obszarach nasłonecznionych. Przeprowadzony eksperyment miał na celu weryfikację, czy zaimplementowane mechanizmy pozwolą na wytworzenie opisanych struktur oraz porównanie jak radzą sobie oba typy organizmów w warunkach wzajemnej konkurencji.

\section{Wyniki}

\begin{figure}[h!]
  \centering
    \includegraphics[width=0.7\textwidth]{3.png}
   \caption{Symulacja 1.}
\end{figure}

W pierwszej symulacji obszary nasłonecznione występowały w postaci kilku koncentrycznych stref. Organizm ``czarny'' był reprezentantem genomu ``bulwiastego'', zaś pozostałe dwa -- ``drzewiastego''. Oba typy podczas wzrostu wytworzyły oczekiwane od nich, charakterystyczne struktury, co potwierdza tezę, iż genom sterujący jedynie zachowaniem pojedyńczej komórki jest wystarczający, by zdefioniować unikalny kształt i budowę złożonego organizmu. Można również zaobserwować, że w tej symulacji organizm jasnozielony wykorzystał swoją zdolność szybkiego rozrostu w wielu kierunkach docierając jako pierwszy do większości pożądanych stref i zasiedlając je.

\begin{figure}[h!]
   \label{fig:Sim2}
  \centering
    \includegraphics[width=0.7\textwidth]{4.png}
   \caption{Symulacja 2.}
\end{figure}

Symulacja druga zawierała jeden duży obszar o wysokim nasłonecznieniu i wiele konkurujących o niego organizmów. Na Rysunku~\ref{fig:Sim2} można zaobserwować szereg zjawisk, które miały miejsce w jej trakcie. W lewym dolnym obszarze znajduje się organizm typu ``drzewiastego'', który został odcięty od wolnej przestrzeni na rozrost. Można zaobserwować wysoki udział komórek wzmacniających i strukturę, którą w rzeczywistym świecie określonoby jako pozwijaną lub skompresowaną. Organizm ten poradził sobie w takich warunkach znacznie gorzej niż typ ``bulwiasty''. W górnym obszarze znajduje się miejsce kontaktu między dwoma organizmami współzawodniczącymi o oświetlony teren. Jego efektem jest wytworzenie na ich styku warstwy wzmacniającej (zamiast wykorzystania tak atrakcyjnej przestrzeni na komórki fotosyntezujące).

Próby potwierdziły, iż zaprojektowany model realizuje postawione przed nim cele i pozwala na symulację wzrostu organizmów wielokomórkowych. Możliwe drogi jego rozwoju to wprowadzenie dodatkowych czynników środowiskowych, obumierania komórek, powstawania nowych organizmów, ewolucji genomu lub podjęcie prób analiz ilościowych wyników opisanych powyżej.

\section{Technologia}

Do implementacji środowiska symulacyjnego wykorzystano język \textbf{Python} w wersji 3.3. Było to uzasadnione możliwością realizacji szybkich prototypów, przydatną ze względu na eksperymentalny i nieprecyzyjnie zdefiniowany cel projektu. Realizacja części wizualizacyjnej oparta była o bibliotekę \textbf{PyGame} pozwalającą na wydajne renderowanie grafiki 2D oraz tworzenie animacji. Symulacja wykorzystywała framework \textbf{SimPy}, zapewniajacy wsparcie dla implementacji organizmów jako autonomicznych agentów, zarządzania ograniczonymi zasobami (w tym wypadku -- wolnymi heksami) oraz obsługę upływu czasu i jego synchronizacji z czasem rzeczywistym.

\section{Instrukcja}

Symulację uruchamiamy poleceniem:
\begin{verbatim}
python ecosystem.py
\end{verbatim}

W pliku \emph{properties.py} można zmodyfikować parametry wpływające na przebieg symulacji. Poniżej opisano najważniejsze z nich:
\begin{verbatim}
HEXES = 25 #ilość heksów na krawędzi
WIDTH = 900 #wysokość w pikselach
HEIGHT = 800 #szerokość w pikselach
HEX_MARGIN = 20 #grubość marginesu danego heksa

ORGANISMS = 1 #ilość organizmów
AVERAGE_LIFETIME = 10.0 #średni czas życia
LIFETIME_DEVIATION = 2.0 #odchylenie standardowe czasu życia
SIMULATION_TIME = 20.0 #czas symulacji

LIGHT_SOURCES = 15 #ilość źródeł światła 
LIGHT_MAX = 1000 #maksymalne natężenie
LIGHT_MIN = 10 #minalne natężenie
LIGHT_DEGRADATION = 50 #szybkość degradacji światła ze względu na odległość od źródła
LIGHT_MAX_SIZE = 5 #maksymalna wielkość obszaru oświetlonego
LIGHT_MIN_SIZE = 2 #minimalna wielkość obszaru oświetlonego
LIGHT_RINGS = 3 #ilość pierścieni światła

#genom organizmów bulwiastych
BLOB = [[1, 1, 1, 24, 672, 1, 24, 672, 1, 24, 62, 1, 0],
        [0.0, 0.5, 0.9, 0.7, 0.5, 0.9, 0.1, 0.2, 0.9, 0.1, 0.1, 0.05, 0.0]]
#genom organizmów drzewiastych
TREE = [[1, 1000, 500, 1, 1, 100, 20, 1, 20, 1, 1, 1, 0],
        [0.0, 0.5, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.2, 0.2, 0.05, 0.0]]
\end{verbatim}

\end{document}
