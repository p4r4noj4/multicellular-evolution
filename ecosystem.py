import properties
import terrain
from SimPy import SimulationStep
import random
import pygame
from cells import Organism, Cell
from pygame import display



class Ecosystem:
    def __init__(self, terrain, sim):
        self.terrain = terrain
        self.cells = dict([(id, Cell(hex, id, self, sim)) for (id, hex) in self.terrain.hexes.items()])
        for cell in self.cells.values():
            cell.light = properties.LIGHT_MIN
            cell.type = properties.EnvType.NONE

        for i in range(properties.LIGHT_SOURCES):
            cell = random.choice(list(self.cells.values()))
            while cell.hex.border_colour == properties.BRIGHT_YELLOW:
                cell = random.choice(list(self.cells.values()))
            light_size = random.randint(properties.LIGHT_MIN_SIZE, properties.LIGHT_MAX_SIZE)
            self.set_light(cell, 0)
            self.generate_light(cell, 1, light_size)

        for i in range(properties.LIGHT_RINGS):
            cell = random.choice(list(self.cells.values()))
            while cell.hex.border_colour == properties.BRIGHT_YELLOW:
                cell = random.choice(list(self.cells.values()))
            inner_light = random.randint(properties.LIGHT_MIN_SIZE-1, properties.LIGHT_MAX_SIZE-1)
            outer_light = random.randint(inner_light+ 1, inner_light+random.randint(1, properties.LIGHT_MAX_SIZE - properties.LIGHT_MIN_SIZE ))
            print(str(inner_light) + " " + str(outer_light))
            self.generate_light(cell, inner_light, outer_light)


    def set_light(self, cell, d_light):
        lower_boundary = properties.LIGHT_MAX - properties.LIGHT_DEGRADATION * (d_light + 1)
        upper_boundary = properties.LIGHT_MAX - properties.LIGHT_DEGRADATION * (d_light)
        if lower_boundary< properties.LIGHT_MIN:
            return
        cell.light = random.randint(lower_boundary, upper_boundary)
        cell.hex.set_border_colour((255, 255, int(200*cell.light/properties.LIGHT_MAX)))
        cell.type = properties.EnvType.SUN

    def generate_light(self, cell, inner_light, outer_light):
        c_x, c_y = cell.id
        for next_cell in self.ring(cell, inner_light, outer_light):
            if next_cell.type == properties.EnvType.NONE:
                x, y= next_cell.id
                if abs(c_x-x)>abs(c_y-y):
                    current_size = abs(c_x-x)
                else:
                    current_size = abs(c_y-y)
                self.set_light(next_cell, current_size)

    def try_add(self, v, id):
        try: v.add(self.cells[id])
        except KeyError: pass

    def ring(self, cell, inner_radius, outer_radius):
        hexes = []
        v = set()
        for i in range(inner_radius, outer_radius):
            hexes.extend(cell.hex.hexes_in_radius(i))
        for id in hexes:
            self.try_add(v, id)
        return v

    def vicinity(self, cell, size=1):
        return self.ring(cell, 1, size+1)

def check_for_exit(event):
    if event.type == pygame.QUIT:
        exit()

class EcosystemSimulation(SimulationStep.SimulationStep):
    def __init__(self):
        window = Window()
        self.ecosystem = Ecosystem(window.terrain, self)
        SimulationStep.SimulationStep.__init__(self)

    def update_display(self):
        try:
            while True:
                event = pygame.event.poll()
                if event.type == pygame.KEYDOWN :
                    if event.key == pygame.K_SPACE:
                        while True:
                            event = pygame.event.wait()
                            if event.type == pygame.KEYDOWN:
                                if event.key == pygame.K_SPACE:
                                    raise PauseEnd
                            check_for_exit(event)
                elif event.type == pygame.NOEVENT:
                    break
                check_for_exit(event)
        except PauseEnd:
            pass
        display.update()

        return

    def run_simulation(self):
        self.initialize()
        for i in range(properties.ORGANISMS):
            organism = Organism(ecosystem=self.ecosystem, sim=self)
            self.activate(organism, organism.live(lifetime=random.gauss(properties.AVERAGE_LIFETIME, properties.LIFETIME_DEVIATION)))
        self.startStepping()
        self.simulate(callback=self.update_display, until=properties.SIMULATION_TIME)

class Window:
    def __init__(self):
        pygame.init()
        display.set_caption("Multicellular Evolution")
        surface = display.set_mode((properties.WIDTH, properties.HEIGHT))
        surface.fill(properties.OLIVE)
        self.terrain = terrain.Terrain(surface, properties.HEXES)
        self.terrain.draw()
        display.flip()

class PauseEnd(Exception):pass


if __name__ == "__main__":
    ecosystem = EcosystemSimulation()
    ecosystem.run_simulation()